# Hubzilla AddOns

## Installing into your hubzilla

This description is analogue to the original description on [how to add addons in hubzilla](https://framagit.org/hubzilla/core/blob/master/install/INSTALL.txt).

* First you should be **on** your website folder

        cd mywebsite

* Then you should clone the addon repository (separately). We'll give this repository 
a nickname of 'gbaddons'.

        util/add_addon_repo https://codeberg.org/brockhaus/hubzilla_addons.git gbaddons

* For keeping the addon tree updated, you should be on your top level website
directory and issue an update command for that repository.

        cd mywebsite 
        util/update_addon_repo gbaddons

